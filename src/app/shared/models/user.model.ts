export class User {
    username: string;
    password: string;
    phoneNumber: Number;
    address: string;
    gender: string;
    dob: string;
    name: string;
    email: string;
    _id: string;
    updatedAt: any;
    createdAt: any;
    status: any;
    role: number;

    constructor(details: any) {
        this.username = details.username || '';
        this.password = details.password || '';
        this.phoneNumber = details.phoneNumber || null;
        this.address = details.address || '';
        this.gender = details.gender || '';
        this.dob = details.dob || '';
        this.name = details.name || '';
        this.email = details.email || '';
        this._id = details._id || '';
        this.updatedAt = details.updatedAt || '';
        this.createdAt = details.createdAt || '';
        this.status = details.status || '';
        this.role = details.role || 2;

    }
}