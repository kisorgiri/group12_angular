import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MsgService } from './services/msg.service';
import { LoaderComponent } from './loader/loader.component';
import { UplaodService } from './services/upload.servvice';
import { SocketService } from './services/socket.service';

@NgModule({
  declarations: [PageNotFoundComponent, HeaderComponent, FooterComponent, LoaderComponent],
  imports: [
    CommonModule
  ],
  providers: [MsgService, UplaodService, SocketService],
  exports: [HeaderComponent, FooterComponent, LoaderComponent]
})
export class SharedModule { }
