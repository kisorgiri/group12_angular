import * as io from 'socket.io-client';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()

export class SocketService {
    socket;
    constructor(){
        this.socket = io.connect(environment.Socekt_URL);
    }
}