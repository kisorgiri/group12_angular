// service in angular 
//  service are class which are deifined by Injectable decorator

// j j class injectabel decorator use garera declare garicha , constructor ma inject hunu parcha

import { Injectable } from "@angular/core";
import { ToastrService } from 'ngx-toastr';

@Injectable() // till date injectable doesnot take any meta data
export class MsgService {

    constructor(public toastr: ToastrService) {

    }

    showSuccess(msg: string) {
        this.toastr.success(msg);
    }
    showInfo(msg: string) {
        this.toastr.info(msg);

    }
    showWarnings(msg: string) {
        this.toastr.warning(msg);

    }
    showError(errorData: any) {
        debugger;
        // check what comes in error
        // show appropriate error
        if (errorData) {
            if (errorData.error) {
                this.toastr.error(errorData.error.msg);
            }
        }
    }


}