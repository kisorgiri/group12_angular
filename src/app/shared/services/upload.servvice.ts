import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()

export class UplaodService {
    url;
    constructor() {
        this.url = environment.Base_URL;
    }


    upload(data: any, files: any, method, entity) {
        let token = localStorage.getItem('token');

        //  use xhttp  to send files in server
        const xhttp = new XMLHttpRequest();
        let formData = new FormData();
        return Observable.create((observer) => {
            if (files) {
                console.log('check type ', files);
                files.forEach((item) => {
                    formData.append('images', item, item.name);
                });
            }
            for (let key in data) {
                formData.append(key, data[key]);
            }

            xhttp.onreadystatechange = () => {
                if (xhttp.readyState == 4) {
                    if (xhttp.status == 200) {
                        observer.next(xhttp.response);
                    } else {
                        observer.error(xhttp.response)
                    }
                }
            }
            let api_url;
            if (method == 'PUT') {
                api_url = `${this.url}${entity}/${data._id}?token=${localStorage.getItem('token')}`;
            } else {
                api_url = `${this.url}${entity}?token=${localStorage.getItem('token')}`;

            }

            xhttp.open(method, api_url, true);
            xhttp.send(formData);
        });
    }
}