import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ProfileComponent } from './profile/profile.component';
import { ChatComponent } from './chat/chat.component';
const userRouting: Routes = [
    {
        path: '',
        component: DashboardComponent,
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    }, {
        path: 'chat',
        component: ChatComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(userRouting)],
    exports: [RouterModule]
})
export class UserRoutingModule {

}