
export class Product {
    name: string;
    brand: string;
    category: string;
    description: string;
    _id: string;

    constructor(details: any) {
        for (let key in details) {
            this[key] = details[key] || '';
        }
    }
}


import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class ProductService {
    url;

    constructor(public http: HttpClient) {
        this.url = environment.Base_URL + 'product/'
    }
    /**
     * this.will call add API for product
     * @param product 
     * @returns observables
     */
    add(product: Product) {
        return this.http.post(this.url, product, this.options());
    }
    list() {
        return this.http.get(this.url, this.options());
    }
    getById(id: string) {
        return this.http.get(`${this.url}${id}`, this.options());
    }
    update(product: Product) {
        return this.http.put(this.url + product._id, product, this.options())
    }

    remove(id: string) {
        return this.http.delete(`${this.url}${id}`, this.options())
    }

    search(product: Product) {
        return this.http.post(this.url + 'search', product, this.options())
    }

    upload(product: Product, files: any) {
        let token = localStorage.getItem('token');

        //  use xhttp  to send files in server
        const xhttp = new XMLHttpRequest();
        let formData = new FormData();
        return Observable.create((observer) => {
            if (files) {
                files.forEach((item) => {
                    formData.append('images', item, item.name);
                });
            }
            for (let key in product) {
                formData.append(key, product[key]);
            }

            xhttp.onreadystatechange = () => {
                if (xhttp.readyState == 4) {
                    if (xhttp.status == 200) {
                        observer.next(xhttp.response);
                    } else {
                        observer.error(xhttp.response)
                    }
                }
            }
            let method = product._id ? 'PUT' : 'POST';
            let url = product._id
                ? `${this.url}${product._id}?token=${token}`
                : `${this.url}?token=${token}`;

            xhttp.open(method, url, true);
            xhttp.send(formData);
        });
    }

    options() {
        let option = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('token'),
            })
        }
        return option;
    }
}