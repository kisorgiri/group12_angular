import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { ProductService, Product } from '../services/product.service';
import { Router } from '@angular/router';
import { UplaodService } from 'src/app/shared/services/upload.servvice';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  submitting: boolean = false;
  product;
  images = [];
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router,
    public uploadService: UplaodService
  ) {
    this.product = new Product({});
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.uploadService.upload(this.product, this.images, 'POST', 'product')
      .subscribe(
        data => {
          this.router.navigate(['/product/list']);
          this.msgService.showSuccess('Proudct Added Successfully');
        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        }
      )
    // 
    // this.productService.add(this.product).subscribe(
    //   data => {
    //     this.router.navigate(['/product/list']);
    //     this.msgService.showSuccess('Proudct Added Successfully');
    //   },
    //   error => {
    //     this.submitting = false;
    //     this.msgService.showError(error);
    //   }
    // )

  }

  fileChanged(file) {
    this.images.push(file.target.files[0])
  }

}
