import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { ProductService } from '../services/product.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  productId;
  product;
  loading: boolean = false;
  submitting: boolean = false;
  imgUrl: string;
  filesToUpload = [];

  constructor(
    public router: Router,
    public activeRoute: ActivatedRoute,
    public msgService: MsgService,
    public productService: ProductService
  ) {
    this.imgUrl = environment.Img_URL;
  }

  ngOnInit() {
    this.loading = true;
    this.productId = this.activeRoute.snapshot.params['id'];
    this.productService.getById(this.productId).subscribe(
      (data: any) => {
        this.loading = false;
        console.log('product data >>', data);
        this.product = data;
        this.product.colors = data.colors.join(',');
        this.product.tags = data.tags.join(',');
        this.product.warrenty = data.warrenty.hasOwnProperty('status') ? true : false;
      },
      err => {
        this.loading = false;
        this.msgService.showError(err);
      }
    )
  }

  submit() {
    this.submitting = true;
    this.productService.upload(this.product, this.filesToUpload).subscribe(
      data => {
        this.submitting = false;
        this.msgService.showSuccess('Product Updated Successfully');
        this.router.navigate(['/product/list']);
      },
      error => {
        this.submitting = false;
        this.msgService.showError(error);
      }
    )
  }
  fileChange(file) {
    this.filesToUpload.push(file.target.files[0]);
  }

}
