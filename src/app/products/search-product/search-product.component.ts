import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { ProductService, Product } from '../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.css']
})
export class SearchProductComponent implements OnInit {
  submitting: boolean = false;
  loading: boolean = false;
  searchResult: boolean = false;
  results = [];
  categories = [];
  product;
  allProducts = [];
  groupByName = [];
  showName: boolean = false;
  showMultipleDate: boolean = false;
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router
  ) {
    this.product = new Product({});
    this.product.category = '';
  }

  ngOnInit() {
    this.productService.list().subscribe(
      (data: any) => {
        this.allProducts = data;
        data.forEach((item) => {
          if (this.categories.indexOf(item.category) == -1) {
            this.categories.push(item.category);
          }
        })
      }
      , error => {
        this.msgService.showError(error)
      }
    )
  }

  search() {

    if (!this.product.toDate) {
      this.product.toDate = this.product.fromDate;
    }
    this.submitting = true;
    this.productService.search(this.product)
      .subscribe(
        (data: any) => {
          this.submitting = false;
          console.log('search result >>>', data);
          if (!data.length) {
            this.msgService.showInfo('No any products matched your search query');
          } else {
            this.searchResult = true;
            this.results = data;
          }
        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        }
      )
  }
  catChange(val) {
    console.log('val is >>', val);
    this.groupByName = this.allProducts.filter((item) => {
      if (item.category == val) {
        return item;
      }
    });
    this.product.name = '';
    this.showName = true;

  }
  showSearchForm(ev) {
    console.log('data is >>>', ev);
    this.searchResult = false;
    this.product = new Product({});
    this.product.category = '';
    this.showMultipleDate = false;

  }

  changeMultipleDate() {
    this.showMultipleDate = !this.showMultipleDate;
    if (!this.showMultipleDate) {
      this.product.toDate = null;
    }
    console.log('thi', this.showMultipleDate);
  }
}
