import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products = [];
  loading: boolean = false;
  @Input() results: any
  @Output() showAgain = new EventEmitter<any>();
  imgUrl;
  constructor(
    public productService: ProductService,
    public router: Router,
    public msgService: MsgService
  ) {
    this.imgUrl = environment.Img_URL;
  }

  ngOnInit() {
    if (!this.results) {
      this.loading = true;
      this.productService.list().subscribe(
        (data: any) => {
          this.loading = false;
          this.products = data;
        },
        error => {
          this.loading = false;
          this.msgService.showError(error);
        }
      )
    } else {
      this.products = this.results;
    }
  }

  remove(id, i) {
    let confirmation = confirm('Are you sure to remove?');
    if (confirmation) {
      this.productService.remove(id)
        .subscribe(
          data => {
            this.msgService.showSuccess('Product deleted successfully');
            this.products.splice(i, 1);
          },
          err => {
            this.msgService.showError(err);
          }
        )
    }

  }
  showSearchMenu() {
    this.showAgain.emit(true);
  }

}
