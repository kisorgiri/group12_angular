import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'group12';
  user;
  constructor(
    public router: Router
  ) {
    this.router.events.subscribe((ev: NavigationEnd) => {
      if (ev.url) {
        var authRoute = ev.url.split('/')[1];
        if (authRoute != 'auth') {
          console.log('here for redirect', localStorage.getItem('token'));
          if (!localStorage.getItem('token')) {
            this.router.navigate(['/auth/login']);
          }
        }
      }
    })
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }

  isLoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    }
    else {
      return false;
    }
  }

}


// @component is a decorator
// selector it will act as html element which will carry whole component


// meta data ===>{}

// this is an object which will define class
// this will supply necessary templates and styles and selector for class(module)

