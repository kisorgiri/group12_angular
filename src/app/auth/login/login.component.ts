import { Component } from "@angular/core";
import { User } from '../../shared/models/user.model';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    user: User;
    submitting: boolean = false;

    constructor(
        public router: Router,
        public msgService: MsgService,
        public authService: AuthService
    ) {
        this.user = new User({});
        if (localStorage.getItem('token')) {
            this.router.navigate(['/user/']);
        }
    }


    login() {


        // this.getNote()
        //     .then((data) => {
        //         console.log('data here', data);
        //     })
        //     .catch((err) => {
        //         console.log('error >> ', err);
        //     })

        // let ram = this.watchGOT()
        //     .subscribe(
        //         (data) => {
        //             console.log("data in ram >>>", data);
        //         },
        //         (error) => {
        //             console.log('error >>', error);
        //         },
        //         (complete) => {
        //             console.log('observable completed >>>', complete)
        //         }
        //     )

        // let shyam = this.watchGOT()
        //     .subscribe(
        //         (data) => {
        //             console.log("data in shyam >>>", data);
        //             if (data == 'episode 5') {
        //                 shyam.unsubscribe();
        //             }
        //         },
        //         (error) => {
        //             console.log('error >>', error);
        //         },
        //         (complete) => {
        //             console.log('observable completed >>>', complete)
        //         }
        //     )



        // this.submitting = true;
        // send this data to BE through service
        this.authService.login(this.user)
            .subscribe(
                (data: any) => {
                    console.log('success >>', data);
                    this.msgService.showSuccess('Welcome ' + data.user.username);
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('user', JSON.stringify(data.user));
                    this.router.navigate(['/user']);
                },
                error => {
                    console.log('failure >>', error);
                    this.msgService.showError(error);
                }
            )


    }
    foucused(a) {
        if (a) {
            console.log('i get focused');
        } else {
            console.log('i lost focues');
        }
    }



}


// promise
// promise has 3 state , 
// pending, onfullfilled, onRejection , settled
//  promise cannot change its state after it is settled 
// promsie can handle single values

// promise cannot be cancelleabel
// promise method
// then, catch, finally



// reactive programming
// reactive programming is programming approach that supports sequence of data over of time

// rxjs reactive extension for javascript
// 
//observables
// Observables are result handling mechanism for asynchrouns code

// observalbel has 2 part 
// 1st part ==> observer
// 2 part subsriber

//observer
// 3 method of working
// next() // to handle success data
// error() // to handle error data
// complete // this will notify completinon of observables

// subscriber part
// 3 methods inside subscriber
// eg 
// .subscribe(
//     (data)=>{

//     },
//     (err)=>{

//     },
//     ()=>{

//     }
// )

// observable can handle multiple values
// observalbes are cancellable
