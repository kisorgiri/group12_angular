import { Injectable } from "@angular/core";
import { User } from 'src/app/shared/models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthService {
    url;
    constructor(
        public http: HttpClient
    ) {
        this.url = environment.Base_URL + 'auth/'
    }

    login(data: User) {
        // return Observable.create((observer) => {
        //     console.log('dat in service >>', data);
        //     this.http.post(this.url + 'login', data, {
        //         headers: new HttpHeaders({
        //             'Content-Type': 'application/json'
        //         })
        //     }).subscribe(
        //         data => {
        //             observer.next(data);
        //         },
        //         error => {
        //             console.log('error in service >>', error);
        //             observer.error(error);
        //         }
        //     );
        // });
        return this.http.post(this.url + 'login', data, this.options());
    }

    regiter(data: User) {
        return this.http.post(this.url + 'register', data, this.options())

    }

    forgotPassword(data: User) {
        return this.http.post(this.url + 'forgot-password', data, this.options());
    }
    resetPassword(token: string, data: User) {
        return this.http.post(this.url + 'reset-password/' + token, data, this.options());

    }

    redirectToHome() {
        if (localStorage.getItem('token')) {
            let user = JSON.parse(localStorage.getItem('user'));
            // if(user.role == 1){
            //     this.
            // }
        }
    }

    options() {
        let option = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        }
        return option;
    }

}