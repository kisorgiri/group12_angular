import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  user;
  token;
  submitting: boolean = false;
  constructor(
    public msgService: MsgService,
    public router: Router,
    public authService: AuthService,
    public activeRoute: ActivatedRoute
  ) {
    this.user = new User({});
  }

  ngOnInit() {
    this.token = this.activeRoute.snapshot.params['token'];
  }

  submit() {
    this.submitting = true;
    this.authService.resetPassword(this.token, this.user).subscribe(
      data => {
        this.msgService.showInfo("Password reset succesfull please login");
        this.router.navigate(['/auth/login']);
      },
      err => {
        this.submitting = false;
        this.msgService.showError(err);
      }
    )
  }

}
