import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/user.model';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  submitting: boolean = false;
  user;
  constructor(
    public authService: AuthService,
    public router: Router,
    public msgService: MsgService
  ) {
    this.user = new User({});
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.authService.forgotPassword(this.user)
      .subscribe(
        data => {

          this.msgService.showInfo('Password reset link sent to email please check your email');
          this.router.navigate(['/auth/login']);
        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        }
      )
  }

}
