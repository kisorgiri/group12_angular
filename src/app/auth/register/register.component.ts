import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/models/user.model';
import { MsgService } from 'src/app/shared/services/msg.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  submitting: boolean = false;
  user: User;
  constructor(
    public msgService: MsgService,
    public authService: AuthService,
    public router: Router
  ) {
    console.log('i am constructor ');
  }

  ngOnInit() {
    this.user = new User({});

    console.log('i will be called 2nd>>>', this.user);
    // call service for BE
    // data
  }

  submit() {
    this.submitting = true;

    this.authService.regiter(this.user)
      .subscribe(

        (data: any) => {
          console.log('data is >>', data);
          this.msgService.showSuccess('Registration successfull please activate your account to login');
          this.router.navigate(['/auth/login']);
        },
        error => {
          this.msgService.showError(error);
        }
      )

  }

  // service code
  register(data: User) {
    // http call
  }

}

// life cycle hooks of component
//  on init 
// on change
// on destroy

// viewAfterInit
// vewBeforeInit

