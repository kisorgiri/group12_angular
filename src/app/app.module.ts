import { BrowserAnimationsModule, } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routing';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { ProductsModule } from './products/products.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    AuthModule,
    UsersModule,
    SharedModule,
    ToastrModule.forRoot(),
    ProductsModule

  ],
  providers: [],
  bootstrap: [AppComponent] // any component that are kept inside bootstrap section is root component
})
export class AppModule {

}
// app module is root module for now
//  module is class



//glossary
// //////################ CLASSS##################
// component
// module
//service
// pipe
// directive
//################CLASS###################

// meta data 
// selector
// decorator

// selector 
// selector will be used as an html element in html file
// selector will be inside component


//decorator
// a decorator is function that define class using meta data
// 26-30 /// all are class but decorator will be different
// every methods that comes with @ prefix is a decorator
// there should not any code between class and decorator 

// meta data ===>{}

// this is an object which will define class
// this will supply necessary component class service pipes for class(module)

// declaration 
// declaration is key in meta data  which will holds array of component,directive,pipes
// component // at the end render hune files,template and styles

// imports
// imports holds all the modules of our application
// imports ma module matrai bascha
// imports will hold 
///1 third party module 
///2 inbuilt module
///3 own module(user defined module)

// providers 
// provider will holds service only
//  service is class defined a decorator(@Injectable)

// bootstrap
// it is used specify root component /// component must be declared inside declaration before placing in bootstrap\

// exports 
// exports will specify what are contain to be exported,

//entryCOmponents 
//  it will accepts components only
//  all modals dialogs components should be place in entry components


// directives are attributes/ element class / comments that will change the behaviour of html