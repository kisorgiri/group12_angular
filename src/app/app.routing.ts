import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';

const appRouting: Routes = [
    {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule' // lazy loading
    },
    {
        path: 'user',
        loadChildren: './users/users.module#UsersModule'
    },
    {
        path: 'product',
        loadChildren: './products/products.module#ProductsModule'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }



]
@NgModule({
    imports: [RouterModule.forRoot(appRouting, { useHash: true })],
    exports: [RouterModule]
})

export class AppRoutingModule {

}

/// revision
// routing >>> routerModule 
// routing config block ==> routes
// router config block should be kept in forRoot method in root module
//  RouterModule.forRoot(config)

// load routing  using element directive router-outlet
// router outlet will work as place holder for every registered componnent in routing config block,
// attr for navigation  routerLink which will perform navigatio from view
// routerLink="/path_to_go" // [routerLink]="['/path_to_go']"
// routing form controller
//  import Router from angular route
//  inject in constructor with access specifers  (eg publi router:Router)
// when injected the injected property will be availabel in this 
// use this.router.navigate(['/path_to_go]);
// 